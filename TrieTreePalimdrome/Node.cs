﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTreePalimdrome
{
    public class Node
    {
        public Dictionary<char, Node> Children { get; set; } = new Dictionary<char, Node>();
        public int Count { get; set; }

        public void Add(string value)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);
            if (!Children.ContainsKey(next)) // ??
            {
                Node node = new Node();
                Children.Add(next, node);
            }
            Children[next].Add(remaining);
        }

        public bool Find(string value)
        {
            if (value.Length == 0)
            {
                return Count > 0;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Children.ContainsKey(next))
            {
                return false;
            }

            return Children[next].Find(remaining);
        }

        public bool Delete(string value)
        {
            if (value == "")
            {
                if (Children.Count != 0)
                {
                    Count = 0;
                    return false;
                }
                return true;
            }
            else
            {
                char next = value[0];
                string remaining = value.Length == 1 ? "" : value.Substring(1);

                if (Children.ContainsKey(next))
                {
                    if (Children[next].Delete(remaining))
                    {
                        Children.Remove(next);
                        if (Children.Count > 0)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        public void PrintToTreeNode(TreeNode treeNode)
        {
            int i = 0;
            foreach (var keyValuePairs in Children)
            {
                treeNode.Nodes.Add(keyValuePairs.Key.ToString());
                keyValuePairs.Value.PrintToTreeNode(treeNode.Nodes[i]);
                i++;
            }
        }

        private static bool IsPalindrome(string str)
        {
            if(str.Length == 1) //если не считаем, что однобуквенное слово является палиндромом
            {
                return false;
            }
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != str[str.Length - i - 1])
                    return false;
            }
            return true;
        }
        public List<string> FindPalindromes(string start)
        {
            List<string> result = new List<string>();
            if (Count > 0)
            {
                result.Add(start);
            }
            foreach (var keyValuePair in Children)
            {
                List<string> tmp = keyValuePair.Value.FindPalindromes(start + keyValuePair.Key);
                foreach (var word in tmp)
                {
                    if (IsPalindrome(word))
                    result.Add(word);
                }
            }
            return result;
        }
    }
}
