﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTreePalimdrome
{
    class Tree
    {
        public Node Root { get; set; } = new Node();

        public void Add(string value)
        {
            Root.Add(value.ToLower());
        }

        public bool Find(string value)
        {
            return Root.Find(value);
        }

        public void PrintToTreeView(TreeView treeView)
        {
            if (Root != null)
            {
                treeView.Nodes.Add("");
                Root.PrintToTreeNode(treeView.Nodes[0]);
            }
        }

        public bool Delete(string value)
        {
            if (Root != null)
            {
                if (Root.Delete(value.ToLower()))
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public List<string> FindPalindromes()
        {
            return Root.FindPalindromes("");
        }
    }
}

