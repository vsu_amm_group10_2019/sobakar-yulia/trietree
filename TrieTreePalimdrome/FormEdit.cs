﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTreePalimdrome
{
    public partial class FormEdit : Form
    {
        FormState FormState;
        public string word;
        public bool OkClose { get; set; } = false;
        public FormEdit(FormState formState)
        {
            InitializeComponent();
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        btnOK.Visible = true;
                        labelDo.Visible = true;
                        textBox.Visible = true;
                        btnOK.Text = "Add";
                        this.Size = new Size(472, 186);
                        dataGridView.Visible = false;
                        this.Text = "Добавить слово"; 
                        break;
                    }
                case FormState.DELETE:
                    {
                        btnOK.Visible = true;
                        labelDo.Visible = true;
                        textBox.Visible = true;
                        btnOK.Text = "Delete";
                        dataGridView.Visible = false;
                        this.Text = "Удалить слово";
                        this.Size = new Size(472, 186);
                        break;
                    }
                case FormState.FIND:
                    {
                        dataGridView.Visible = true;
                        btnOK.Visible = false;
                        labelDo.Visible = false;
                        textBox.Visible = false;
                        this.Text = "Слова-палиндромы";
                        this.Size = new Size(283, 401);
                        break;
                    }
            }
        }

        public void ShowPalindromes(List <string> data)
        {
            dataGridView.Rows.Clear();
            dataGridView.RowCount = data.Count;
            int i = 0;
            foreach (var elem in data)
            {
                dataGridView.Rows[i].Cells[0].Value = elem;
                i++;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (textBox.Text == "")
            {
                MessageBox.Show("Введите слово!");
            } else
            {
                word = textBox.Text;
                OkClose = true;
                this.Close();
            }
        }

        private void textBox_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            char letter = e.KeyChar;
            if (!Char.IsLetter(letter) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
        }
    }
}
