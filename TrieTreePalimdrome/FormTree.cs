﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTreePalimdrome
{
    public partial class FormTree : Form
    {
        Tree Tree = new Tree();
        public FormTree()
        {
            InitializeComponent();
        }
        private void Redraw()
        {
            treeView.Nodes.Clear();
            Tree.PrintToTreeView(treeView);
            treeView.ExpandAll();
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tree = new Tree();
            openFileDialogTxt.ShowDialog();
            string fileName = openFileDialogTxt.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(new[] { ' ', ',', ':', '?', '!', '-'});
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
        }

        private void addWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEdit add = new FormEdit(FormState.ADD);
            add.ShowDialog();
            if (add.OkClose)
            {
                Tree.Add(add.word);
                Redraw();
            }
        }

        private void deliteWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEdit delete = new FormEdit(FormState.DELETE);
            delete.ShowDialog();
            if (delete.OkClose)
            {
                if (!Tree.Delete(delete.word))
                {
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Не найдено");
                }
            }
        }

        private void findPalindromesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<string> result = Tree.FindPalindromes();
            FormEdit palindromes = new FormEdit(FormState.FIND);
            if (result.Count == 0)
            {
                MessageBox.Show("Слова палиндромы не найдены");
            } else
            {
                palindromes.ShowPalindromes(result);
                palindromes.ShowDialog();
            }  
        }
    }
}
